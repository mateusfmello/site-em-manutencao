# Sorry, under maintenance.
![Print da página de aviso](http://pichoster.net/images/2018/03/03/608a5f7780a2ce53f25e6a36c1b0094e.png "Página de aviso")

Uma simples página HTML e um arquivo .htaccess para deixar um aviso de manutenção no site que possivelmente estará sendo acessado por algum visitante.

## Plus

* Você pode configurar no `.htaccess` o IP externo da sua rede, isso faz com que o aviso de manutenção não seja exibido para acessos feitos através da sua rede.

## Configurando o .htaccess

1. Procure a linha `# RewriteCond %{REMOTE_ADDR} !^127\.0\.0\.1 # Se não for desse IP` e altere o IP para o IP externo atual da sua rede e remova o `#` (hastag/cerquilha/sustenido) do inicio da linha. Ficará algo como:
> `RewriteCond %{REMOTE_ADDR} !^8\.8\.8\.8 # Se não for desse IP`
>
>> OU
>
> `RewriteCond %{REMOTE_ADDR} !^208\.67\.220\.220 # Se não for desse IP`

## Como usar

### Instalação

1. Você precisará do arquivo `manutencao.html` e `.htaccess` então efetue o download da forma que você desejar;
2. Envie o arquivo `manutencao.html` para a raiz da pasta pública do seu servidor de hospedagem;
3. Certifique-se que o arquivo `manutencao.html` está no local correto acessando a seguinte URL:
> http://example.com/manutencao.html
>
> **Copie e cole:** /manutencao.html
4. Agora é a vez do `.htaccess`, porém ele tem uma particularidade.
	1. Caso já exista um arquivo `.htaccess` na raiz, recomendo que copie o conteúdo e cole no topo do arquivo (não precisa/deve apagar o conteúdo original).
	2. Se não existir basta enviá-lo para raiz.
	> **Após isso seu site já estará exibindo o aviso para os visitantes**

### Remoção

1. A remoção é bem simples.
	1. Para o **passo 4.1** da instalação basta você comentar as linhas que você adicionou no `.htaccess`. Para isso insira `#` (hastag/cerquilha/sustenido) no inicio de cada linha adicionada.
	2. Para o **passo 4.2** da instalação basta você deletar o arquivo `.htaccess`, porém **recomendo** que você comente as linhas assim como no passo 1.1 ou então renomeie o arquivo para algo como `manutencao.htaccess`. Assim quando você voltar a dar manutenção não terá muito trabalho;
2. Não existe necessidade de fazer qualquer ação no arquivo `manutencao.html`, pois ele é inofensivo e só será visto. Exceto caso seja acessado usando a URL absoluta dele (http://example.com/manutencao.html).

# Meu IP externo

* [Site 1](http://meuip.com)
* [Site 2](https://www.whatismyip.com)
* [Site 3](https://whatismyip.com.br)
* [Site 4](https://whatismyipaddress.com)
* [Site 5](http://e-meuip.com)

# Contato:
[Mateus Fernandes Web Developer](https://mfmello.com)
